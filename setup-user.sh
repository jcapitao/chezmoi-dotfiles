#!/bin/bash

set -e
exec 2> >(while read line; do echo -e "\e[01;31m$line\e[0m"; done)

dotfiles_dir="$(
    cd "$(dirname "$0")"
    pwd
)"
cd "$dotfiles_dir"

link() {
    orig_file="$dotfiles_dir/$1"
    if [ -n "$2" ]; then
        dest_file="$HOME/$2"
    else
        dest_file="$HOME/$1"
    fi

    mkdir -p "$(dirname "$orig_file")"
    mkdir -p "$(dirname "$dest_file")"

    rm -rf "$dest_file"
    ln -s "$orig_file" "$dest_file"
    echo "$dest_file -> $orig_file"
}

is_chroot() {
    ! cmp -s /proc/1/mountinfo /proc/self/mountinfo
}

systemctl_enable_start() {
    echo "systemctl --user enable --now "$1""
    systemctl --user enable --now "$1"
}

clone_project() {
    local repo_url=$1
    local path=$2
    if [ ! -d "$path" ]; then
        echo "git clone $repo_url $path"
        git clone $repo_url $path
    fi
}

flatpak_install() {
    local flatpak=$1
    if ! flatpak list | grep -q -e "$flatpak"; then
        echo "Installing $flatpak..."
        flatpak install -y -u "$flatpak"
    fi
}

flatpak_override() {
    local flatpak=$1
    shift 1
    local opts=$@
    if flatpak list | grep -q -e "$flatpak"; then
        echo "Overriding $flatpak with $opts"
	flatpak override -u $flatpak $opts
    fi
}

echo "==========================="
echo "Setting up user dotfiles..."
echo "==========================="

link ".aliases"
link ".bashrc"
link ".config/systemd/user/container-syncthing.service"
link ".centos.cert"
link ".centos-jcapitao.crt"
link ".centos-jcapitao.key"
link ".centos-server-ca.cert"
link ".exports"
link ".fedora.upn"
link ".functions"
link ".gitconfig"
link ".git-completion.bash"
link ".local/bin/backup"
link ".local/bin/clamshell"
link ".local/bin/build-rpm"
link ".local/bin/crosstag"
link ".local/bin/dotfiles"
link ".local/bin/fedpkg_clone"
link ".local/bin/scan"
link ".local/bin/switch-mode"
link ".local/bin/waybar-vpn"
link ".openrc.sh"
link ".rpm-extra"
link ".tmux.conf"
link ".tmuxifier_layouts/rdo-branch.window.sh"
link ".tmuxifier_layouts/rdo-build.window.sh"
link ".tmuxifier_layouts/rdo-puppet.window.sh"
link ".tmuxifier_layouts/rdo-release.session.sh"

link ".config/alacritty"
link ".config/keepassxc"
link ".config/libvirt"
link ".config/nvim/init.vim"
link ".config/python-bugzilla"
link ".config/rofi"
link ".config/rpkg"
link ".config/sway"
link ".config/syncthing"
link ".config/udiskie"
link ".config/vifm"
link ".config/waybar"
link ".config/zathura"
link ".ssh"


if is_chroot; then
    echo >&2 "=== Running in chroot, skipping user services..."
else
    echo ""
    echo "================================="
    echo "Enabling and starting services..."
    echo "================================="

    systemctl --user daemon-reload
    #systemctl_enable_start "container-syncthing.service"

    echo "==================="
    echo "Cloning projects..."
    echo "==================="

    clone_project "https://github.com/jimeh/tmuxifier.git" "$HOME/.config/tmuxifier"

    echo "==================="
    echo "Install Flatpak apps..."
    echo "==================="

    flatpak remote-add -u --if-not-exists flathub https://dl.flathub.org/repo/flathub.flatpakrepo
    flatpak_install fedora org.keepassxc.KeePassXC
    flatpak_install com.brave.Browser
    flatpak_install md.obsidian.Obsidian
    flatpak_install org.videolan.VLC
    flatpak_install org.chromium.Chromium
    flatpak_install com.transmissionbt.Transmission
    flatpak_install com.slack.Slack
    flatpak_install org.ferdium.Ferdium
    flatpak_install org.libreoffice.LibreOffice
    flatpak_install org.gnucash.GnuCash
    flatpak_override org.gnucash.GnuCash --env GTK_THEME=Adwaita:dark
fi

