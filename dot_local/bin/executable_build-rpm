#!/bin/bash

# Script to build packages
#
# example:
#       build_pkg -t <release>
#       build_pkg -t rawhide -v -d
#
# Return:
#       0 if local AND scratch build are successful
#       1 if local build is not successful
#       2 if local build is successful AND scratch build not successful
#       3 if local build is successful AND scratch build not requested
#       4 else

source $HOME/.rpm-extra
ts=$(date +"%Y-%m-%d-%H-%M-%S")
stat logs >/dev/null 2>&1 || mkdir logs
exec > >(tee -i logs/logfile-$ts)
exec 2>&1

KOJI_USERNAME=jcapitao
MOCK_CONFIG_DIR=$(dirname $(dirname $0))/mock/
PKG=$(basename $(pwd))
RPMBUILDDIR=$PWD/rpmbuild/
VERBOSE_LEVEL='0'
LOCAL_BUILD=true
SCRATCH_BUILD=true
DRY_RUN_MODE=false

function usage(){
	printf "Usage: $(basename $0) -t $MASTER_RELEASE\n"
    printf "\t-v  : increase verbosity.\n"
    printf "\t-t  : the tag against you whish to build.\n"
    printf "\t-L  : do not request local build.\n"
    printf "\t-K  : do not request scratch build.\n"
    printf "\t-d  : run in dry-run mode.\n"
	exit 4
}

if [ $# -eq 0 ]; then
    usage
fi

optstring=":t:vLKd"
while getopts ${optstring} arg; do
    case ${arg} in
        t)
          TAG="${OPTARG}"
          ;;
        v)
          VERBOSE_LEVEL='1'
          ;;
        L)
          LOCAL_BUILD=false
          ;;
        K)
          SCRATCH_BUILD=false
          ;;
        d)
          DRY_RUN_MODE=true
          ;;
        ?)
          echo "Invalid option: -${OPTARG}."
          echo
          usage
          ;;
    esac
done

if [[ $TAG =~ ^f[0-9][0-9] ]]; then
    koji_tag=$TAG
    dist=$(echo $TAG | sed 's/\(.\)\(..\)/\1c\2/')
    fedora_version=$(echo $TAG | sed 's/\(.\)\(..\)/\2/')
    mock_configfile=fedora-$fedora_version-x86_64
    koji_target=$TAG
    koji_profile=fedora
elif [[ $TAG == "rawhide" ]]; then
    koji_tag=rawhide
    print_out 1 "Fetching current dist value for rawhide"
    dist=fc$(find /etc/mock/ -name fedora* -type l -print | head -n 1 | cut -d- -f2)
    mock_configfile=fedora-rawhide-x86_64
    koji_target=rawhide
    koji_profile=fedora
    enable_koji_repo="--enablerepo=local"
elif [[ $TAG == "cs10" ]]; then
    koji_tag=""
    dist=el10s
    mock_configfile=centos-stream-10-x86_64
    koji_target=""
    koji_profile=cbs
elif [[ $TAG == "epoxy" ]]; then
    TAG=epoxy
    koji_tag=cloud9s-openstack-$TAG-el9s-build
    dist=el9s
    mock_configfile=cloud9s-openstack-$TAG-el9s-build-x86_64
    koji_target=cloud9s-openstack-$TAG-el9s
    koji_profile=cbs
elif [[ $TAG == "dalmatian" ]]; then
    TAG=dalmatian
    koji_tag=cloud9s-openstack-$TAG-el9s-build
    dist=el9s
    mock_configfile=cloud9s-openstack-$TAG-el9s-build-x86_64
    koji_target=cloud9s-openstack-$TAG-el9s
    koji_profile=cbs
elif [[ $TAG == "caracal" ]]; then
    TAG=caracal
    koji_tag=cloud9s-openstack-$TAG-el9s-build
    dist=el9s
    mock_configfile=cloud9s-openstack-$TAG-el9s-build-x86_64
    koji_target=cloud9s-openstack-$TAG-el9s
    koji_profile=cbs
elif [[ $TAG == "bobcat" ]]; then
    TAG=bobcat
    koji_tag=cloud9s-openstack-$TAG-el9s-build
    dist=el9s
    mock_configfile=openstack-$TAG-9-x86_64
    koji_target=cloud9s-openstack-$TAG-el9s
    koji_profile=cbs
elif [[ $TAG == "antelope" ]]; then
    TAG=antelope
    koji_tag=cloud9s-openstack-$TAG-el9s-build
    dist=el9s
    mock_configfile=centos-stream-9-x86_64
    koji_target=cloud9s-openstack-$TAG-el9s
    koji_profile=cbs
elif [[ $TAG == "zed" ]]; then
    TAG=zed
    koji_tag=cloud9s-openstack-$TAG-el9s-build
    dist=el9s
    mock_configfile=centos-stream-9-x86_64
    koji_target=cloud9s-openstack-$TAG-el9s
    koji_profile=cbs
elif [[ $TAG == "yoga8" ]]; then
	TAG=yoga
    koji_tag=cloud8s-openstack-$TAG-el8-build
    dist=el8
    mock_configfile=centos-stream-8-x86_64
    koji_target=cloud8s-openstack-$TAG-el8
    koji_profile=cbs
elif [[ $TAG == "yoga9" ]]; then
    TAG=yoga
    koji_tag=cloud9s-openstack-$TAG-el9s-build
    dist=el9s
    mock_configfile=centos-stream-9-x86_64
    koji_target=cloud9s-openstack-$TAG-el9s
    koji_profile=cbs
elif [[ $TAG == "train7" ]]; then
    TAG=train
    koji_tag=cloud-openstack-$TAG-el7-build
    dist=el7
    mock_configfile=centos-7-x86_64
    koji_target=cloud7-openstack-$TAG-el7
    koji_profile=cbs
elif [[ $TAG == "train8" ]]; then
    TAG=train
    koji_tag=cloud8s-openstack-$TAG-el8-build
    dist=el8
    mock_configfile=centos-stream-8-x86_64
    koji_target=cloud8s-openstack-$TAG-el8
    koji_profile=cbs
elif [[ $TAG == "victoria8" ]]; then
    TAG=victoria
    koji_tag=cloud8s-openstack-$TAG-el8-build
    dist=el8
    mock_configfile=centos-stream-8-x86_64
    koji_target=cloud8s-openstack-$TAG-el8
    koji_profile=cbs
elif [[ $TAG == "xena8" ]]; then
    TAG=xena
    koji_tag=cloud8s-openstack-$TAG-el8-build
    dist=el8
    mock_configfile=centos-stream-8-x86_64
    koji_target=cloud8s-openstack-$TAG-el8
    koji_profile=cbs
elif [[ $TAG == "xena9" ]]; then
    TAG=xena
    koji_tag=cloud9s-openstack-$TAG-el9s-build
    dist=el9s
    mock_configfile=centos-stream-9-x86_64
    koji_target=cloud9s-openstack-$TAG-el9s
    koji_profile=cbs
elif [[ $TAG == "wallaby8" ]]; then
    TAG=wallaby
    koji_tag=cloud8s-openstack-$TAG-el8-build
    dist=el8
    mock_configfile=centos-stream-8-x86_64
    koji_target=cloud8s-openstack-$TAG-el8
    koji_profile=cbs
elif [[ $TAG == "wallaby9" ]]; then
    TAG=wallaby
    koji_tag=cloud9s-openstack-$TAG-el9s-build
    dist=el9s
    mock_configfile=centos-stream-9-x86_64
    koji_target=cloud9s-openstack-$TAG-el9s
    koji_profile=cbs
elif [[ $TAG == "nfv9" ]]; then
    koji_tag=nfv9s-openvswitch-2-el9s-build
    dist=el9s
    mock_configfile=centos-stream-9-x86_64
    koji_target=nfv9s-openvswitch-2-el9s
    koji_profile=cbs
elif [[ $TAG == "nfv10" ]]; then
    koji_tag=nfv10s-openvswitch-2-el10s-build
    dist=el10s
    mock_configfile=centos-stream-10-x86_64
    koji_target=nfv10s-openvswitch-2-el10s
    koji_profile=cbs
elif [[ $TAG == "opstools9s" ]]; then
    koji_tag=opstools9s-collectd-5-el9s-build
    dist=el9s
    mock_configfile=centos-stream-9-x86_64
    koji_target=opstools9s-collectd-5-el9s
    koji_profile=cbs
elif [[ $TAG == "opstools8s" ]]; then
    koji_tag=opstools8s-collectd-5-el8s-build
    dist=el9s
    mock_configfile=centos-stream-8-x86_64
    koji_target=opstools8s-collectd-5-el8s
    koji_profile=cbs
elif [[ $TAG == "epel8" ]]; then
    koji_tag=epel8-build
    dist=el8
    mock_configfile=centos-stream+epel-8-x86_64
    koji_target=epel8
    koji_profile=fedora
elif [[ $TAG == "epel9" ]]; then
    koji_tag=epel9-build
    dist=el9
    mock_configfile=centos-stream+epel-9-x86_64
    koji_target=epel9
    koji_profile=fedora
elif [[ $TAG == "py311" ]]; then
    koji_tag=rawhide
    dist=fc$(find /etc/mock/ -name fedora* -type l -print | head -n 1 | cut -d- -f2)
    mock_configfile=fedora-rawhide-python311
    koji_target=rawhide
    koji_profile=fedora
else
    koji_tag=cloud9s-openstack-$TAG-el9s-build
    dist=el9s
    mock_configfile=centos-stream-9-x86_64
    koji_target=cloud9s-openstack-$TAG-el9s
    koji_profile=cbs
fi

ls *.spec >/dev/null 2>&1
if [ ! $? -eq 0 ]; then
    print_out 0 "There is no spec file to build in current directory"
    exit 1
fi

if [ -n "$koji_tag" ]; then
    print_out 1 "The script will work with $koji_profile koji profile"
    print_out 1 "Fetching latest build of $PKG in $koji_profile..."
    latest_build=$(koji -p $koji_profile latest-build --quiet $koji_tag $PKG | awk '{print $1}')
    print_out 1 "Latest build on '$koji_tag' tag is: $latest_build"
fi

mkdir -p $RPMBUILDDIR/SPECS \
         $RPMBUILDDIR/BUILD \
         $RPMBUILDDIR/SOURCES \
         $RPMBUILDDIR/BUILDROOT \
         $RPMBUILDDIR/SRPMS \
         $RPMBUILDDIR/RPMS
echo "%_topdir $RPMBUILDDIR" > ~/.rpmmacros

rm $RPMBUILDDIR/SOURCES/* >/dev/null 2>&1
# Download remote sources (only) with debug enabled
download_sources=$(spectool -g -S -D -C $RPMBUILDDIR/SOURCES *.spec 2>/dev/null)
print_out 1 $download_sources
remote_sources=$(ls -rt --color=never $RPMBUILDDIR/SOURCES)
# Get local sources
local_sources=$(rpmspec -q -P *.spec 2>/dev/null | grep -e "^Source" | awk '{print $2}' | grep -v -e "^http")
if [ -n "$local_sources" ]; then cp $local_sources $RPMBUILDDIR/SOURCES; fi
# TODO: handle better local patches
ls *.patch >/dev/null 2>&1 && cp *.patch $RPMBUILDDIR/SOURCES
ls *.diff >/dev/null 2>&1 && cp *.diff $RPMBUILDDIR/SOURCES
rm $RPMBUILDDIR/SPECS/* >/dev/null 2>&1
cp *.spec $RPMBUILDDIR/SPECS
print_out 0 "Build SRPM init \tOK"

pushd $RPMBUILDDIR >/dev/null
rm SRPMS/* >/dev/null 2>&1
srpm_build=$(rpmbuild --define "dist .$dist" -bs SPECS/*.spec 2>&1)
srpm_filename=$(ls --color=never SRPMS/)
if [ -n "$srpm_filename" ]; then
    srpm_file=${RPMBUILDDIR}/SRPMS/$srpm_filename
    print_out 0 "Build SRPM \t\tOK => $srpm_filename"
else
    print_out 0 "Error while building SRPM"
    print_out 0 "$srpm_build"
    exit 1
fi
popd >/dev/null
rm ~/.rpmmacros

# build locally before requesting a scratch build
if $LOCAL_BUILD ; then
    #cp -R $(dirname $(dirname $0))/mock $HOME/.config
    if [[ $VERBOSE_LEVEL == "0" ]]; then VERBOSE_CMD="-q"; fi
    print_out 0 "Local build \t\t[START]"
    mock -r $mock_configfile $srpm_file $VERBOSE_CMD $enable_koji_repo
    RET_CODE=$?
    mkdir local_build >/dev/null 2>&1
    pushd local_build >/dev/null
    mkdir -p $ts >/dev/null 2>&1
    rm current >/dev/null 2>&1
    ln -s $ts current
    cp /var/lib/mock/$mock_configfile/result/* current/
    popd >/dev/null
    if [ $RET_CODE -eq 0 ]; then
        print_out 0 "Local build \t\t[SUCCESS]"
    elif [ $RET_CODE -eq 1 ]; then
        print_out 0 "Local build \t\t[ERROR] \nSee details in local_build/current"
        exit 1
    else
        print_out 0 "Local build \t\t[ERROR] \nSee details in local_build/current"
        exit 4
    fi
else
    print_out 0 "Local build \t\t[SKIPPED]"
fi

#scratch_build=false
if $SCRATCH_BUILD ; then
    assert_koji_authentication
    build_on_koji --scratch
    RET_CODE=$?
    if [ $RET_CODE -eq 0 ]; then
        if [ $koji_profile == "fedora" ]; then
            cp $RPMBUILDDIR/SOURCES/* $RPMBUILDDIR/SRPMS/* .
            > /tmp/sources_$srpm_file_name
            for remote_source in $remote_sources; do
                git status -s | grep $remote_source >/dev/null 2>&1 && echo "/$remote_source" >> .gitignore
                echo "SHA512 ($remote_source) = $(sha512sum $remote_source | awk '{print $1}')" >> /tmp/sources_$srpm_file_name
                if ! $DRY_RUN_MODE ; then
                    fedpkg new-sources $remote_source
                fi
            done
            if $DRY_RUN_MODE ; then
                print_out 0 "Sources are not pushed upstream as dry-run mode in ON."
            fi
            mv /tmp/sources_$srpm_file_name sources
        elif [ $koji_profile == "cbs" ] && [ ! $DRY_RUN_MODE ]; then
            read -p "Do you want to request the final build on CBS? [yes/no] " -N 3 -r
            echo    # (optional) move to a new line
            if [[ $REPLY =~ ^yes$ ]]; then
                echo "final buil not requested for security"
                build_on_koji
                RET_CODE=$?
                if [ $RET_CODE -eq 0 ]; then
                    print_out 1 "CBS build OK."
                    print_out 1 "Don't forget to push your changes remotely:"
                    print_out 1 "git checkout -b $MASTER_RELEASE-c8-rdo"
                    print_out 1 "git commit -am ''"
                    print_out 1 "git remote add upstream git@github.com:rdo-common/python-project_name.git"
                    print_out 1 "git push upstream $MASTER_RELEASE-c8-rdo"
                else
                    exit 2
                fi
            fi
        fi
    else
        rm -rf $RPMBUILDDIR
        exit 2
    fi
    rm -rf $RPMBUILDDIR
    exit 0
else
    print_out 0 "Scratch build \t\t[SKIPPED]"
    exit 3
fi
