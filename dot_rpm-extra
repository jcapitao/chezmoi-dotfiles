#!/bin/bash

function get_trunk_comp_url {
    local release=$1
    local centos_v=$2
    local component=$3
    local url=""
    url="c${centos_v}-${release}-${component},https://trunk.rdoproject.org/centos${centos_v}-${release}/component/${component}/current/"
    echo $url
}

function get_trunk_comp {
    local release=$1
    local centos_v=$2
    local trunk_url="https://trunk.rdoproject.org/centos${centos_v}-${release}/"
    local comps=""
    comps=$(curl -L -f --silent $trunk_url/current/delorean.repo | grep -e "^\[delorean" | sed "s/\[delorean-component-\([a-zA-Z0-9]*\)]/\1/" | tr "\n" " ")
    echo -e "$comps"
}

function get_trunk_comp_urls {
    local release=$1
    local centos_v=$2
    local all_urls=""
    for comp in $(get_trunk_comp $release $centos_v); do
        url=$(get_trunk_comp_url $release $centos_v $comp)
        all_urls="$all_urls $url"
    done
    echo $all_urls
}

function get_trunk_urls {
    local release=$1
    local centos_v=$2
    local deps="c${centos_v}-${release}-deps,https://trunk.rdoproject.org/centos${centos_v}-${release}/deps/latest/"
    local build_deps="c${centos_v}-${release}-build-deps,https://trunk.rdoproject.org/centos${centos_v}-${release}/build-deps/latest/"
    local comp_urls=$(get_trunk_comp_urls $release $centos_v)
    echo "$deps $build_deps $comp_urls"
}

function get_centos_repo_urls {
    local centos_v=$1
    if [ "$centos_v" == "9" ]; then
        echo -e "appstream,https://mirror.stream.centos.org/9-stream/AppStream/x86_64/os/ baseos,https://mirror.stream.centos.org/9-stream/BaseOS/x86_64/os/ crb,https://mirror.stream.centos.org/9-stream/CRB/x86_64/os/ nfv,https://mirror.stream.centos.org/9-stream/NFV/x86_64/os/ ha,https://mirror.stream.centos.org/9-stream/HighAvailability/x86_64/os/ rabbitmq,http://mirror.stream.centos.org/SIGs/9-stream/messaging/x86_64/rabbitmq-38/ storage,http://mirror.stream.centos.org/SIGs/9-stream/storage/x86_64/ceph-quincy/ opstools,http://mirror.stream.centos.org/SIGs/9-stream/opstools/x86_64/collectd-5/ nfv-ovs,http://mirror.stream.centos.org/SIGs/9-stream/nfv/x86_64/openvswitch-2/"
    fi
}

function repofrompath_command_builder {
    local prefix="tmp"
    local repos=""
    for repo in $@; do
        repo_name=$(echo $repo | cut -d, -f1)
        repo_url=$(echo $repo | cut -d, -f2)
        repos="$repos --repofrompath=${prefix}-${repo_name},${repo_url}"
    done
    echo "--disablerepo=* --enablerepo=${prefix}* $repos"
}

function rdowhatprovides {
    local release=$1
    local centos_v=$2
    local pkg=$3
    if [ $# -lt 3 ]; then
        echo -e "Usage: rdowhatprovides master 9 python3-oslo-log [options]"
        echo -e "       rdowhatprovides master 9 python3-oslo-log --refresh"
	return 1
    fi
    shift 3
    local opts="${@}"
    local trunk_urls=""

    trunk_urls=$(get_trunk_urls $release $centos_v)
    centos_urls=$(get_centos_repo_urls $centos_v)
    opts="$(repofrompath_command_builder $centos_urls $trunk_urls) $opts"
    dnf whatprovides $pkg $opts
}

function rdowhatrequires {
    local release=$1
    local centos_v=$2
    local pkg=$3
    if [ $# -lt 3 ]; then
        echo -e "Usage: rdowhatrequires master 9 python3-pbr [options]"
        echo -e "Usage: rdowhatrequires master 9 python3-pbr --refresh"
	return 1
    fi
    shift 3
    local opts="${@}"
    local trunk_urls=""

    trunk_urls=$(get_trunk_urls $release $centos_v)
    centos_urls=$(get_centos_repo_urls $centos_v)
    opts="$(repofrompath_command_builder $centos_urls $trunk_urls) $opts"
    repoquery --whatrequires $pkg $opts
}

function rdorequires {
    local release=$1
    local centos_v=$2
    local pkg=$3
    if [ $# -lt 3 ]; then
        echo -e "Usage: rdorequires master 9 python3-pbr [options]"
        echo -e "Usage: rdorequires master 9 python3-pbr --refresh"
	return 1
    fi
    shift 3
    local opts="${@}"
    local trunk_urls=""

    trunk_urls=$(get_trunk_urls $release $centos_v)
    centos_urls=$(get_centos_repo_urls $centos_v)
    opts="$(repofrompath_command_builder $centos_urls $trunk_urls) $opts"
    repoquery --requires $pkg $opts
}

rdocloneupstream () {
    git_url=$(rdopkg findpkg -s $1 | grep upstream | awk '{print $2}')
    [[ -z "$git_url" ]] && echo "The package '$1' does not exist" && return 1
    ls $1 >/dev/null 2>&1
    if [[ $? -eq 0 ]]; then
        pushd $1
        git stash
        git pull
    else
        git clone $git_url $1 && pushd $1
    fi
}

promote_builds () {
    bash $HOME/workspace/rdo-toolbox/scripts/promote_builds.sh $1 $2 $3 $4
}

download_sources () {
    spectool -g *.spec -C $DEST_DIR ${1:-.}
}

fedinit () {
    KRB5_TRACE=/dev/stdout kinit jcapitao@FEDORAPROJECT.ORG
}

build_pkg () {
    bash $HOME/workspace/rdo-toolbox/scripts/build_pkg.sh $@
}

add_pkg_to_candidate () {
    bash $HOME/workspace/rdo-toolbox/scripts/add_pkg_to_candidate.sh $@
}

show_deps_update () {
    bash $HOME/workspace/rdo-toolbox/scripts/show_deps_update.sh $@
}

download_job_logs () {
    bash $HOME/workspace/rdo-toolbox/scripts/download_job_logs.sh $@
}

unfold_fedpkg () {
    bash $HOME/workspace/rdo-toolbox/scripts/unfold_fedpkg.sh $@
}


#!/bin/bash
LATEST_RELEASE=victoria
MASTER_RELEASE=wallaby
CURRENT_RELEASE=wallaby
NEXT_RELEASE=xena


# Those variables below should be removed in the future
RDOINFO_GIT_URL="https://github.com/redhat-openstack/rdoinfo.git"
RELENG_GIT_URL="https://github.com/redhat-openstack/rdoinfo.git"
GATING_SCRIPTS_GIT_URL="ssh://jcapiitao@review.rdoproject.org:29418/gating_scripts"

function assert_bin_available() {
    for bin in "$@"; do
        if ! hash $bin 2>/dev/null
        then
            echo "'$bin' was not found in PATH"
            exit 1
        fi
    done
}

function assert_koji_authentication {
    koji -p $koji_profile call getSessionInfo >/dev/null 2>&1
    if [ ! $? -eq 0 ]; then
        print_out 0 "Please authenticate yourself against koji server"
        if [ $koji_profile == "fedora" ]; then
            # Fedora uses kerberos for authentication
            KRB5_TRACE=/dev/stdout kinit $KOJI_USERNAME@FEDORAPROJECT.ORG
        elif [ $koji_profile == "cbs" ]; then
            # CBS uses ssl for authentication
            centos-cert --verify
            print_out 0 "See https://wiki.centos.org/HowTos/CentosPackager"
            print_out 0 "Run the command below:"
            print_out 0 "centos-cert -u $KOJI_USERNAME -n"
            exit 1
        fi
	fi
}

function build_on_koji {
    local mode=$1
    local workdir="koji_build"
    [ ! -d ${workdir} ] && mkdir -p ${workdir}
    cp $srpm_file $workdir
    [ $mode == "--scratch" ] && mode_display="scratch" || mode_display="final"

    print_out 0 "$koji_profile $mode_display build \t\t[START]"
    koji -p $koji_profile build --wait $mode $koji_target $srpm_file | tee $workdir/task.out
    task_id=$(grep buildArch $workdir/task.out | head -1 | awk '{print $1}')
    tail -1 $workdir/task.out >> $workdir/results.out
    grep -e "build.log" $workdir/task.out | grep -e "http://cbs.centos.org/" | sed 's/\ //g' > $workdir/build.log
    grep -e "root.log" $workdir/task.out | grep -e "http://cbs.centos.org/" | sed 's/\ //g' > $workdir/root.log
    koji -p $koji_profile taskinfo $task_id | grep rpm$ >> $workdir/generated_packages.txt
    tail -1 $workdir/task.out | grep -e "successfully" >/dev/null 2>&1
    if [ $? -eq 0 ]; then
        print_out 0 "$koji_profile $mode_display build \t\t[SUCCESS]\n"
        return 0
    else
        print_out 0 "$koji_profile $mode_display build \t\t[ERROR] \nSee details in $PWD/$workdir"
        return 1
    fi
}

function clone_project_repo() {
    local repo_name=$1
    local branch=${2:-"master"}
    local orphan=${3:-"false"}

    ls $repo_name >/dev/null 2>&1
    if [ $? -eq 0 ]; then
        pushd $repo_name >/dev/null
        if [ $orphan == "false" ]; then
            checkout_status=$(git -c advice.detachedHead=false checkout $branch 2>&1)
        else
            checkout_status=$(git checkout --orphan $branch 2>&1)
        fi
        if [ $? -eq 0 ]; then
            echo -e "Checking out to '$branch' \tOK"
        else
            echo -e "Checking out to '$branch' \tERROR"
            echo "$checkout_status"
            return 1
        fi
        popd >/dev/null
    else
        clone_status=$(git clone ${projects_git_url[$repo_name]} 2>&1)
        if [ $? -eq 0 ]; then
            echo -e "Cloning git project '$repo_name' \tOK"
            clone_project_repo $1 $2 $3
        else
            echo "Cloning git project '$repo_name' \tERROR"
            echo "$clone_status"
            return 1
        fi
    fi
}

function print_out {
    local THIS_LEVEL=$1
    shift
    local MESSAGE="${@}"
    if [[ "${THIS_LEVEL}" -le "${VERBOSE_LEVEL}" ]]; then
       printf "${MESSAGE}\n"
    fi
}

function source_virtualenv {
    local venv_name=".venv"
    local python_bin=${1:-python3}

    set +e
    ls $venv_name >/dev/null 2>&1
    if [ $? -ne 0 ]; then
        venv_creation=$(virtualenv -p /usr/bin/$python_bin $venv_name)
        if [[ ! $? -eq 0 ]]; then
            echo $venv_creation
            return 1
        fi
    fi
	set -e

	source $venv_name/bin/activate
    if [[ "$(which python)" == "$PWD/$venv_name/bin/python" ]]; then
        pip install --upgrade pip >/dev/null 2>&1
        print_out 0 "Sourcing virtualenv '$venv_name' with $python_bin \tOK"
    else
        return 1
    fi
}

function extract_git_repo_from_pypi {
    local project=$1
    curl -s -H "Accept: application/json" "https://pypi.org/pypi/$project/json" > $TMP_FILE
    repo_url=$(cat $TMP_FILE | sed -n 's/^.*\(https:\/\/opendev\.org\/[a-zA-Z0-9/]*\)\\n.*/\1/p')
    if [ -n $repo_url ]; then
        echo $repo_url
        return 0
    fi
}

function rdogetcomponents() {
    if [ $# -ne 1 ]; then
        echo -e "Usage: rdogetcomponents centos9-master"
        echo -e "       rdogetcomponents centos9-zed"
	return 1
    fi
    req=$(curl -L -f --silent https://trunk.rdoproject.org/$1/component 2>&1)
    rc=$?
    if [ $rc -ne 0 ]; then
        echo -e "An error occured when fetching components against trunk.rdoproject.org"
	echo -e "curl rc: $rc"
	return 1
    fi
    comps=$(echo -e "$req" | grep -e '\[DIR\]' | sed 's/.*href="\(.*\)\/".*/\1/')
    echo $comps
}

function rdorepoquery {
    if [ $# -lt 2 ]; then
        echo -e "This is a wrapper for dnf and repoquery commands."
        echo -e "It adds the repos in --repofrompath= based on the repo files living in Trunk"
        echo -e "Usage: rdorepoquery centos9-master dnf whatprovides python3-pbr"
        echo -e "       rdorepoquery centos9-zed repoquery --whatrequires python3-pbr"
	return 1
    fi
    local rel=$1
    shift
    local cmd=$@
    local trunk_url="https://trunk.rdoproject.org/$rel/"
    comps=$(rdogetcomponents $rel)
    centos_ver=$(echo $rel | cut -d- -f1)
    local repofrompaths=""
    if [ "$centos_ver" == "centos9" ];then
        repofrompaths="--repofrompath=tmp-appstream,https://mirror.stream.centos.org/9-stream/AppStream/x86_64/os/ --repofrompath=tmp-baseos,https://mirror.stream.centos.org/9-stream/BaseOS/x86_64/os/ --repofrompath=tmp-crb,https://mirror.stream.centos.org/9-stream/CRB/x86_64/os/"
    elif [ "$centos_ver" == "centos8" ];then
        repofrompaths="--repofrompath=tmp-appstream,https://mirror.stream.centos.org/8-stream/AppStream/x86_64/os/ --repofrompath=tmp-baseos,https://mirror.stream.centos.org/8-stream/BaseOS/x86_64/os/ --repofrompath=tmp-powertools,https://mirror.stream.centos.org/8-stream/powertools/x86_64/os/"
    else
        echo "Please set centos9-<release> or centos8-<release>"
    fi
    for c in $comps; do
	repofrompaths="$repofrompaths --repofrompath=tmp-component-$c,$trunk_url/component/$c/current/"
    done
    repo=$(curl -L -f --silent $trunk_url/dlrn-deps.repo 2>&1)
    rc=$?
    if [ $rc -ne 0 ]; then
        echo -e "An error occured when fetching dlrn-deps repo against trunk.rdoproject.org"
	echo -e "curl rc: $rc"
	return 1
    fi
    deps_repofrompaths=$(echo -e "$repo" | grep -e "^\[" -e "^baseurl=" | sed "s/\[/\ --repofrompath=tmp-/;s/\]//;s/^baseurl=/,/" | tr -d '\n')
    repofrompaths="$repofrompaths $deps_repofrompaths"
    echo -e "Running the command below:"
    $cmd --disablerepo=* --enablerepo=tmp* $repofrompaths
}

switch_to_pyproject_macros() {
    local specfile=$1
    if [ ! -n "$specfile" ]; then
        specfile=$(ls --color=no *.spec 2>/dev/null)
        specfile="$(realpath $specfile)"
    fi
    if [ ! -n "$specfile" ]; then
        echo "No SPEC file found. Exit."
	return 1
    fi
    sed -i "s/Source0:\([[:space:]]*\).*/Source0:\1%{pypi_source}/" $specfile
    sed -i "s/%py3_build/%pyproject_wheel/" $specfile
    sed -i "s/%py3_install/%pyproject_install/" $specfile
}

is_fedora_project() {
    local project="$1"
    local url=""
    local http_code=""

    if [ ! -n "$project" ]; then
        project=$(basename $PWD)
    fi
    url="https://src.fedoraproject.org/api/0/rpms/$project"
    http_code=$(curl -o /dev/null -s -w "%{http_code}\n" $url)
    if [ "$http_code" == "200" ]; then
        echo -e "$project"
        return 0
    else
        echo -e "Error: It looks like $url is not a Fedora project."
        return 1
    fi
}

fedpkg_get_latest_build_nvr_in_rawhide() {
    local project="$1"
    local is_fedora=""
    local build_nvr=""

    project=$(is_fedora_project $project)
    if [ $? -ne 0 ]; then
        echo $project
	return 1
    fi

    build_nvr=$(koji latest-build --quiet rawhide $project | awk '{print $1}')
    if [ -n "$build_nvr" ]; then
        echo -e "$build_nvr"
        return 0
    else
        echo -e "$build_nvr"
        return 1
    fi
}

fedpkg_get_latest_build_url_in_rawhide() {
    local project="$1"
    local build_nvr=""
    local build_info=""
    local build_task=""

    build_nvr=$(fedpkg_get_latest_build_nvr_in_rawhide $1)
    if [ $? -ne 0 ]; then
        echo -e "Could not get the latest koji build NVR"
        echo -e "$build_nvr"
        return 1
    fi

    build_info=$(koji buildinfo $build_nvr)
    if [ $? -ne 0 ]; then
        echo -e "An error occurred when querying the build info"
        echo -e "$build_info"
        return 1
    fi

    build_task=$(echo -e "$build_info" | grep -e "^Task" | awk '{print $2}')
    if [ $? -eq 0 ]; then
        echo -e "https://koji.fedoraproject.org/koji/taskinfo?taskID=$build_task"
        return 0
    fi
}

fedpkg_closed_rawhide() {
    local project="$1"
    local build_url=""
    local bug_id=""
    local ret_1=""
    local ret_2=""
    
    build_url=$(fedpkg_get_latest_build_url_in_rawhide $project)
    ret_1=$?
    bug_id=$(bz_find_is_available_ticket $project)
    ret_2=$?
    if [ $ret_1 -eq 0 ] && [ $ret_2 -eq 0 ]; then
        output=$(bugzilla modify $bug_id -a jcapitao@redhat.com --close RAWHIDE --comment "Built with $build_url")
        if [ $? -eq 0 ]; then
            echo -e "https://bugzilla.redhat.com/show_bug.cgi?id=$bug_id commented and closed rawhide with the latest build task url"
            return 0
        else
            echo -e "An error occurred when updateing the BZ ticket."
            echo -e "$output"
            return 1
        fi
    fi
}

bz_find_is_available_ticket() {
    local project="$1"
    local is_fedora=""
    local bugs=""
    local bug_id=""

    project=$(is_fedora_project $project)
    if [ $? -ne 0 ]; then
        echo $project
	return 1
    fi

    bugs=$(bugzilla query -p "Fedora" -c $project -s NEW,OPEN | grep -i -e "$project.* is available")
    bug_id=$(echo -e "$bugs" | sed 's/#\([0-9]*\) .*/\1/')
    if [ -n "$bug_id" ]; then
        echo -e "$bug_id"
        return 0
    else
        return 1
    fi
}

