source ~/workspace/release-rdo/common.rc
PUPPET_WORKSPACE=puppet_modules/build

new_window "puppet"
run_cmd "toolbox enter toolbox"
run_cmd "source common.rc"
run_cmd "ls $PUPPET_WORKSPACE >/dev/null 2>&1 || mkdir -p $PUPPET_WORKSPACE"
run_cmd "cd $PUPPET_WORKSPACE"
run_cmd "PKG=puppet-staging"
run_cmd "export PATH=\$PATH:\$RELENG_LOCAL_PATH/scripts/new_release_scripts/"
run_cmd "clear"
run_cmd "#echo \$PKG > /tmp/build_puppet_snap && create_build_snap.sh \$PKG"
split_h 50
run_cmd "toolbox enter toolbox"
run_cmd "source common.rc"
run_cmd "clear"
run_cmd "find /tmp/build_puppet_snap | entr -s 'rdopkg findpkg \$(cat /tmp/build_puppet_snap)'"
select_pane 1
