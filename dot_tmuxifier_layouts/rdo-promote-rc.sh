RELEASE=victoria
PREV_RELEASE=ussuri

new_window "promote-rc"
run_cmd "PKG=puppet-staging && echo \$PKG > /tmp/build_puppet_snap"
run_cmd "ls build_puppet_snap >/dev/null 2>&1 || mkdir -p build_puppet_snap"
run_cmd "pushd build_puppet_snap"
run_cmd "ls releng >/dev/null 2>&1 || git clone git@github.com:rdo-infra/releng.git"
run_cmd "pushd releng"
run_cmd "git fetch ssh://jcapiitao@review.rdoproject.org:29418/rdo-infra/releng refs/changes/14/15114/8 && git checkout FETCH_HEAD"
run_cmd "sed -i 's/^RELEASE=.*/RELEASE=$RELEASE/g' scripts/new_release_scripts/create_build_snap.sh"
run_cmd "sed -i 's/^RELEASE_TAG=.*/RELEASE_TAG=$RELEASE-uc/g' scripts/new_release_scripts/create_build_snap.sh"
run_cmd "sed -i 's/^PREV_RELEASE=.*/PREV_RELEASE=$PREV_RELEASE/g' scripts/new_release_scripts/create_build_snap.sh"
run_cmd "chmod u+x scripts/new_release_scripts/create_build_snap.sh"
run_cmd "export PATH=\$PATH:\$PWD/scripts/new_release_scripts/"
run_cmd "popd"
run_cmd "PKG=puppet-staging"
run_cmd "#echo \$PKG > /tmp/build_puppet_snap && create_build_snap.sh \$PKG"
split_h 50
run_cmd "pushd build_puppet_snap"
run_cmd "#cbs list-tagged cloud8-openstack-$PREV_RELEASE-release | grep \$(cat /tmp/build_puppet_snap)"
split_v 50
run_cmd "pushd build_puppet_snap"
run_cmd "#cbs add-pkg cloud8-openstack-$RELEASE-candidate --owner=rdobuilder \$(cat /tmp/build_puppet_snap)"
run_cmd "#cbs tag-build cloud8-openstack-$RELEASE-candidate build_package"
select_pane 1
