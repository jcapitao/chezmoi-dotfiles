source ~/workspace/release-rdo/common.rc
WORKSPACE=build

new_window "build"
run_cmd "toolbox enter toolbox"
run_cmd "source common.rc"
run_cmd "ls $WORKSPACE >/dev/null 2>&1 || mkdir -p $WORKSPACE"
run_cmd "cd $WORKSPACE"
run_cmd "export PATH="\$PATH:\$HOME/workspace/releng/scripts/new_release_scripts/""
run_cmd "clear"
run_cmd "PKG=python-stevedore"
run_cmd "#echo \$PKG > /tmp/build_pkg && send_new_version.sh \$PKG"
split_h 50
run_cmd "toolbox enter toolbox"
run_cmd "source common.rc"
run_cmd "cd build"
run_cmd "clear"
run_cmd "find /tmp/build_pkg | entr sh -c 'rdopkg findpkg \$(cat /tmp/build_pkg)'"
split_v 50
run_cmd "toolbox enter toolbox"
run_cmd "source common.rc"
run_cmd "cd build"
run_cmd "clear"
run_cmd "#crosstag \$(cat /tmp/build_pkg) $LATEST_RELEASE $MASTER_RELEASE"
split_v 20
run_cmd "toolbox enter toolbox"
run_cmd "source common.rc"
run_cmd "cd build"
run_cmd "clear"
run_cmd "watch -n 300 'rdo_projects -b \$MASTER_RELEASE-rdo -r \$MASTER_RELEASE -m cloud9s-openstack-\$MASTER_RELEASE-candidate |wc -l'"
select_pane 1
